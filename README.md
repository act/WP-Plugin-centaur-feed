Wordpress plugin to load JSON exports from centaur publications
# Author URI: http://research.reading.ac.uk/act
# Licence
This plugin has been design for the sole internal use of the University of Reading. Any use outside the university scope must have prior agreement.