(function() {
	tinymce.PluginManager.add( 'centaur_class', function( editor, url ) {
		// Add Button to Visual Editor Toolbar
		editor.addButton('centaur_class', {
			title: 'Insert CentAUR JSON export',
			image: url + '/icon.png',
			cmd: 'centaur_class',
		});
		// Add Command when Button Clicked
		editor.addCommand('centaur_class', function() {
			/*
			// Ask the user to give centAUR JSON link
			var result = prompt('Give the CentAUR JSON export URL. Then click OK.');
			if ( !result ) {
				// User cancelled - exit
				return;
			}
			if (result.length === 0) {
				// User didn't enter a text - exit
				return;
			} 
		
			text = '<br> [centaur url=\"' + result +'\"] <br>' ;*/
			
			text = '<br> [centaur url=\"\" exclude=\"\" dateorder=\"DESC\" ids=\"\"] <br>' ;
			// Insert selected text back into editor, wrapping it in an anchor tag
			editor.execCommand('mceReplaceContent', false, text);
		});	
	});
})();
