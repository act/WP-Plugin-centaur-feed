/*
This script loads JSON exports from centaur and displays them in the associated divs.
*/

jQuery(function($) { 
	i = 1;
	var jsonurl = new Array();
	var div = new Array();
	var dateorder =  new Array();
	var exclude =  new Array();
	var id = new Array();
	var loader = new Array();
	
	while ( typeof (window['cf_vars'+i]) !== 'undefined' ){ //loop on all shortcode variables (if more than one shortcode in page)
		/*console.log("URL: " + window['cf_vars'+i].url);
		console.log("exclude: " + window['cf_vars'+i].exclude);
		console.log("dateorder: " + window['cf_vars'+i].dateorder);
		console.log("IDs: " + window['cf_vars'+i].ids);
		console.log("div: " + window['cf_vars'+i].div);
		console.log("------------------");*/
	
		//Div and loaders IDs have to be saved in an array and called after each getJSON action has been completed as those are asynchonous and it wouldn't be possible to designate each DIV or loader using the i loop counter only:
		jsonurl[i] = window['cf_vars'+i].url;
		exclude[i] = window['cf_vars'+i].exclude;
		dateorder[i] = window['cf_vars'+i].dateorder;
		id[i] = window['cf_vars'+i].ids.replace(" ", "").split(",");
		div[i] =  "#" + window['cf_vars'+i].div;
		loader[i] = "#" + window['cf_vars'+i].loader;
		
		
		//If we have a json url to read:
		if (jsonurl[i] != "") {
			//console.log("loading json export: " +jsonurl[i]);
			$.getJSON(jsonurl[i] + "?&callback=?", 
				(function(j) { //j will take the current i value and become a local variable
					return function(data){
						var items = [];
						year = 0;
						newyear = 0;
						sortcentaur(data,dateorder[j]);
						
						$.each( data, function( key, val ) {
							if ( exclude[j].indexOf(String(val.eprintid)) == -1) {//Test if the publication isn"t excluded
								year = Number(String(val.date).substring(0,4));//get the year of the publication to display
								if ( ( year != newyear ) && ( dateorder[j] != "" ) ) items.push ("<tr><td><h2>" + year + "</h2></td></tr>"); //display the year as a title
								items.push ("<tr><td>" + display_one_publication(val) + "</td></tr>");
								newyear = year;
							}
						});
						$("<table/>", {
							"class": "publications",
							html: items.join("")
							}).appendTo(div[j]);
						$(loader[j]).hide();
					};
				}(i))
			)
			.fail(function() {
				console.log("Unable to read the CentAUR JSON export at: " + jsonurl[i]) ;
			}); 
		}
		//If we want to load publications ID per ID:
		else if (id[i][0] != ""){
			//console.log("loading publications by ID...");
			var m = 0; //loop counter for all listed IDs
			for (m=0; m < id[i].length; m++){
				jsonurl2 = "https://centaur.reading.ac.uk/cgi/search/archive/advanced/export_reading_JSON.js?screen=Search&dataset=archive&_action_export=1&output=JSON&exp=0%7C1%7C-date%2Fcreators_sort_name%2Ftitle%7Carchive%7C-%7Cnas_itemid%3Aeprintid%3AALL%3AEQ%3A" + id[i][m] + "%7C-%7Ceprint_status%3Aeprint_status%3AANY%3AEQ%3Aarchive%7Cmetadata_visibility%3Ametadata_visibility%3AANY%3AEQ%3Ashow&n=&cache=1";
				$.getJSON(jsonurl2 + "&callback=?", 
					(function(k) {
						return function(data2){
							$.each( data2, function( key, val ) {
								$(div[k]).append("<table class='publications'><tr><td>" + display_one_publication(val) + "</td></tr></table>");//call function to display one publication (one table line)
							});
							$(loader[k]).hide();
						};
					}(i))
				)
				.fail(function() {
					console.log("Unable to read the CentAUR JSON export by ID at URL " + jsonurl2) ;
				});
			}
		}
		i++;
		
	}
});	


function sortcentaur(publications,order){
	if (order == "ASC"){
		publications.sort(function(a, b){
			return  Number(String(a.date).substring(0,4)) - Number(String(b.date).substring(0,4));
		});
	}
	else {
		publications.sort(function(a, b){
			return  Number(String(b.date).substring(0,4)) - Number(String(a.date).substring(0,4));
		});
	}
    return publications;
}	

function display_one_publication(publication){
	text = "";
	jQuery.each( publication.creators_sort, function( key, val ) {
		if ( val.id ) {
			text = text + "<a href=\"https://centaur.reading.ac.uk/view/creators/" + val.id + ".html\" target=\"_new\">";
			text = text + val.name.family +", " + val.name.given + ", "; //author with link
			text = text + "</a>";
		}
		else text = text + val.name.family + ", " + val.name.given + ", "; //author
	});
	text = text + "(" + String(publication.date).substring(0,4) + ") "; //year
	text = text + "<a href=\""+publication.uri+"\" target=\"_new\">"+publication.title+"</a>. "; //Title
	if ( publication.publication ) text = text + publication.publication + ", ";		//Journal
	else if ( publication.publisher ) text = text + publication.publisher + ", ";		//Journal
	//Journal
	if ( publication.place_of_pub ) text = text + publication.place_of_pub +". ";		//Place of Publication
	if ( publication.volume ) text = text + publication.volume + " ";		//Volume
	if ( publication.number ) text = text + "(" + publication.number + "). ";		//Number
	if ( publication.pagerange ) text = text + "pp. " + publication.pagerange + ". ";		//Pages
	if ( publication.event_title ) text = text + "In: " + publication.event_title + ", "; //Event title
	if ( publication.event_dates ) text = text + publication.event_dates + ", "; //Event dates
	if ( publication.event_location ) text = text + publication.event_location + ". "; //Event location
	if ( publication.isbn ) text = text + "ISBN " + publication.isbn + ". ";		//ISBN
	if ( publication.citation_extra ) text = text + "(" + publication.citation_extra + ") "; //citation extra
	if ( publication.rioxx2_version_of_record ) text = text + "doi: <a href=\"" + publication.rioxx2_version_of_record + "\" target=\"_new\">" + publication.rioxx2_version_of_record + "</a> "; //doi link
	if ( publication.ispublished ) {
		if ( publication.ispublished === "inpress" ) text = text + "(In Press)"; 
		if ( publication.ispublished === "unpub" ) text = text + "(Unpublished)"; 
	}
	return text;
}
