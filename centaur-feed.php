<?php
/**
 * Plugin Name: CentAUR Feed
 * Plugin URI: http://research.reading.ac.uk/act
 * Version: 1.1
 * Author: Eric MATHIEU
 * Author URI: http://research.reading.ac.uk/act
 * Description: Plugin to read CentAUR JSON exports and display them, using shortcodes.
 * License: Internal use for the University of Reading. Any use outside the university should get prior authorization.
 */
 
 
// ==============================================
//  Prevent Direct Access to this file
// ==============================================

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if this file is accessed directly

$dataToBePassed = array(); //declare global variable to be used several times if multiple shortcodes

class centaur_feed_Class  {
	/**
	* Constructor. Called when the plugin is initialised.
	*/
	function __construct() {
		if ( is_admin() ) {
			add_action( 'init', 'setup_centaur_feed_plugin' );
		}
		// Add shortcode
		add_shortcode('centaur', array($this, 'centaur_shortcode'));
		
	}
	
	/**
	 * CentAUR feed shortcode
	 */
	public function centaur_shortcode($atts, $content = null) {
		static $cfCounter = 0;
		$cfCounter++;
		$parameters = shortcode_atts(array(
			'url' => '',
			'exclude' => '',
			'ids' => '',
			'dateorder' => '',
		), $atts, 'centaur');
		

		$output = '<div id="loader-centaur'.$cfCounter.'" ><div class="loader-centaur"></div>Loading centaur publications...</div><div id="publications-list'.$cfCounter.'"></div>';
		
		// Register the script
		wp_register_script( 'cf-script', plugins_url( '/centaur-feed.js', __FILE__ ), array( 'jquery-core' ), filemtime(plugin_dir_path( __FILE__ ) . '/centaur-feed.js'), true );
		
		//populate the variable to pass the data to the JS script
		$dataToBePassed = array(
				'url' => $parameters['url'],
				'dateorder' => $parameters['dateorder'],
				'exclude' => $parameters['exclude'],
				'ids' => $parameters['ids'],
				'div' => 'publications-list'.$cfCounter,
				'loader' => 'loader-centaur'.$cfCounter
		);
		wp_localize_script( 'cf-script', 'cf_vars'.$cfCounter , $dataToBePassed );
		 
		// Enqueued script with localized data.
		wp_enqueue_script( 'cf-script' );

		
		return $output;
		
	}
}

$centaur_feed = new centaur_feed_Class;




//--------------------------------------------
// Load css
function cf_styles(){
    wp_register_style( 'cf-style', plugins_url( '/centaur-feed.css', __FILE__ ), array(), filemtime(plugin_dir_path( __FILE__ ) . '/centaur-feed.css'), 'all' );
    wp_enqueue_style( 'cf-style' );
}
add_action( 'wp_enqueue_scripts', 'cf_styles' );

//--------------------------------------------
/*
* Check if the current user can edit Posts or Pages, and is using the Visual Editor
* If so, add some filters so we can register our plugin
*/
function setup_centaur_feed_plugin() {
	// Check if the logged in WordPress User can edit Posts or Pages
	// If not, don't register our plugin
		
	if ( ! current_user_can( 'edit_posts' ) && ! current_user_can( 'edit_pages' ) ) {
				return;
	}

	// Setup some filters
	add_filter( 'mce_external_plugins', 'add_centaur_feed_plugin2'  );
	add_filter( 'mce_buttons', 'add_tinymce_toolbar_button2'  );
}


//--------------------------------------------
/*
* Adds a TinyMCE plugin compatible JS file to the TinyMCE / Visual Editor instance
*
* @param array $plugin_array Array of registered TinyMCE Plugins
* @return array Modified array of registered TinyMCE Plugins
*/
function add_centaur_feed_plugin2( $plugin_array ) {
	$plugin_array['centaur_class'] = plugin_dir_url( __FILE__ ) . 'centaur-feed-mce.js';
	return $plugin_array;
}


//--------------------------------------------
/*
* Adds a button to the TinyMCE / Visual Editor which the user can click
* to insert a link with a custom CSS class.
*
* @param array $buttons Array of registered TinyMCE Buttons
* @return array Modified array of registered TinyMCE Buttons
*/
function add_tinymce_toolbar_button2( $buttons ) {
	array_push( $buttons, '|', 'centaur_class' );
	return $buttons;
}


